/* Soal No.1(Range) */
console.log('(Range)');

function range(startNum, finishNum) {
    var arr = [];
    if (startNum == null || finishNum == null) {
        return -1;
    } else if (startNum > finishNum) {
        for (var flag = startNum; flag >= finishNum; flag--) {
            arr.push(flag);
        }
    } else {
        for (var flag = startNum; flag <= finishNum; flag++) {
            arr.push(flag);
        }
    }

    return arr;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

/* Soal No.2(Range with Step) */
console.log('\n(Range with Step)')

function rangeWithStep(startNum, finishNum, step) {
    var arr = [];
    if (startNum == null || finishNum == null) {
        return -1;
    } else if (startNum > finishNum) {
        for (var flag = startNum; flag >= finishNum; flag -= step) {
            arr.push(flag);
        }
    } else {
        for (var flag = startNum; flag <= finishNum; flag += step) {
            arr.push(flag);
        }
    }

    return arr;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

/* Soal No. 3 (Sum of Range) */
console.log('\n(Sum of Range)')

function sum(startNum, finishNum, step) {
    var arr = [];
    if (step != undefined) {
        if (startNum > finishNum) {
            for (var flag = startNum; flag >= finishNum; flag -= step) {
                arr.push(flag);
            }
        } else {
            for (var flag = startNum; flag <= finishNum; flag += step) {
                arr.push(flag);
            }
        }
    } else {
        if (startNum == null) {
            return 0;
        } else if (finishNum == null) {
            return startNum;
        } else if (startNum > finishNum) {
            for (var flag = startNum; flag >= finishNum; flag--) {
                arr.push(flag);
            }
        } else {
            for (var flag = startNum; flag <= finishNum; flag++) {
                arr.push(flag);
            }
        }
    }

    return arr.reduce(function (a, b) {
        return a + b;
    });
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

/* Soal No.4(Array Multidimensi) */
console.log('\n(Array Multidimensi)');
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(arrData) {
    for (var flag = 0; flag < arrData.length; flag++) {
        console.log('Nomor ID: ' + arrData[flag][0]);
        console.log('Nama Lengkap: ' + arrData[flag][1]);
        console.log('TTL: ' + arrData[flag][2]);
        console.log('Hobi: ' + arrData[flag][3] + '\n');
    }
}

console.log(dataHandling(input));

/* Soal No. 5 (Balik Kata) */
console.log('\n(Balik Kata)');

function balikKata(words) {
    var reversedString = "";
    var arr = Array.from(words);
    var arrLength = arr.length;
    for (var i = arrLength - 1; i >= 0; i--) {
        reversedString = reversedString + words.charAt(i);
    }

    return reversedString;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

/* Soal No. 6 (Metode Array) */
console.log('\n(Metode Array)');

function dataHandling2(arr) {
    var firstName = arr[1];
    arr.splice(1, 1, "Roman Alamsyah Elsharawy");
    arr.splice(2, 1, "Provinsi Bandar Lampung");
    arr.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(arr);

    var strDate = arr[3];
    var arrDate = strDate.split("/");
    console.log(month(parseInt(arrDate[1])));
    var arrDateNumber = arrDate.map(Number);
    arrDateNumber.sort(function (value1, value2) {
        return value2 - value1;
    });
    console.log(arrDateNumber.map(String));
    console.log(arrDate.join("-"));
    console.log(firstName);

}

function month(bulan) {
    switch (bulan) {
        case 1:
            return 'Januari';
            break
        case 2:
            return 'Februari';
            break;
        case 3:
            return 'Maret';
            break;
        case 4:
            return 'April';
            break;
        case 5:
            return 'Mei';
            break;
        case 6:
            return 'Juni';
            break;
        case 7:
            return 'Juli';
            break;
        case 8:
            return 'Agustus';
            break;
        case 9:
            return 'September';
            break;
        case 10:
            return 'Oktober';
            break;
        case 11:
            return 'November';
            break;
        case 12:
            return 'Desember';
            break;
        default:
            return "Maaf tidak terdapat bulan dengan urutan tsb di calendar";
            break;
    }
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);