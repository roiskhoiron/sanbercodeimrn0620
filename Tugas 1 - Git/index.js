/* A.Tugas String */
// No.1
console.log("\n=================== No 1 ===================")
var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';

console.log(word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh)

// No.2
console.log("\n=================== No 2 ===================")
var sentence = "I am going to be React Native Developer";

var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12];
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First Word: ' + exampleFirstWord);
console.log('Second Word: ' + exampleSecondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);
console.log('Sixth Word: ' + sixthWord);
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord)

// N0.3
console.log("\n=================== No 3 ===================")
var sentence2 = 'wow JavaScript is so cool';

var exampleFirstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 27);

console.log('First Word: ' + exampleFirstWord2);
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);

// NO.4
console.log("\n================== No 4 ====================")
var sentence3 = 'wow JavaScript is so cool';

var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 27);

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength);
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength);
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength);
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);

/* B. Tugas Conditional */
console.log("\n================== Conditional Test ====================")
console.log("\n================== Conditional IF-ELSE ====================")
var nama = "John"
var peran = ""

if (nama == '' && peran == '') { // Output untuk Input nama = '' dan peran = ''
    console.log("Nama harus diisi!")
} else if (nama == 'John' && peran == '') { //Output untuk Input nama = 'John' dan peran = ''
    console.log("Halo John, Pilih peranmu untuk memulai game!")
} else if (nama == 'Jane' && peran == 'Penyihir') { //Output untuk Input nama = 'Jane' dan peran 'Penyihir'
    console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'Jenita' && peran == 'Guard') { //Output untuk Input nama = 'Jenita' dan peran 'Guard'
    console.log("Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Junaedi' && peran == 'Werewolf') { //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
    console.log("Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}

console.log("\n================== Conditional Switch-Case ====================")
var tanggal = 25; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 4; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1998; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch (bulan) {
    case 1:
        bulan = 'Januari';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break
    case 2:
        bulan = 'Februari';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 3:
        bulan = 'Maret';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 4:
        bulan = 'April';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 5:
        bulan = 'Mei';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 6:
        bulan = 'Juni';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 7:
        bulan = 'Juli';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 8:
        bulan = 'Agustus';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 9:
        bulan = 'September';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 10:
        bulan = 'Oktober';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 11:
        bulan = 'November';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    case 12:
        bulan = 'Desember';
        console.log(tanggal + ' ' + bulan + ' ' + tahun);
        break;
    default:
        console.log("Maaf tidak terdapat bulan ke-" + bulan + " di calendar")
        break;
}
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945';