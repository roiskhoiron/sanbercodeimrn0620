/* No.1 Looping while */
console.log('LOOPING PERTAMA');
var flag = 2;
while (flag <= 20) {
    console.log(flag + ' - I love Coding');
    flag += 2;
}

console.log('\nLOOPING KEDUA');
flag = 20;
while (flag >= 2) {
    console.log(flag + ' - I Will Become a Mobile Developer');
    flag -= 2;
}

/* No.2 Looping for */
console.log('\n LOOPING for')
for (var flag = 1; flag <= 20; flag++) {

    if (flag % 2 == 0) {
        console.log(flag + ' - Berkualitas');
    } else if (flag % 3 == 0) {
        if (flag % 2 == 1) {
            console.log(flag + ' - I Love Coding');
        }
    } else {
        console.log(flag + ' - Santai');
    }
}

/* No.3 Membuat Persegi Panjang# */
console.log('\nPersegi Panjang 8 x 4');
for (var row = 1; row <= 4; row++) {
    var colmn = 1;
    do {
        process.stdout.write('#');
        colmn++;
    } while (colmn < 8);
    process.stdout.write('#\n');
}

/* No.4 Membuat Tangga */
console.log('\nMembuat Tangga 7 level');
for (var level = 1; level <= 7; level++) {
    var flag = 0;
    do {
        process.stdout.write('#');
        flag++;
    } while (flag < level);
    process.stdout.write('\n');
}

/* No. 5 Membuat Papan Catur */
console.log('\nMembuat Papan Catur 8 x 8');
var hitam = "#"
var putih = " ";
var hasil = "";
var num = 8;
for (var row = 0; row < num; row++) {
    for (var colmn = 0; colmn < num; colmn++) {
        if (row % 2 == 1) {
            if (colmn % 2 == 0) {
                hasil = hasil + hitam;
            } else {
                hasil = hasil + putih;
            }
        } else {
            if (colmn % 2 == 0) {
                hasil = hasil + putih;
            } else {
                hasil = hasil + hitam;
            }
        }
    }
    console.log(hasil);
    hasil = "";
}